﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOAD_CouponsProject
{
    public partial class AddBusinessOwnerWindow : Form
    {
        public AddBusinessOwnerWindow()
        {
            InitializeComponent();
        }

        private void AddBusineesOwnerButton_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            int Id = Convert.ToInt32(ownerBusinessIdTextBox.Text);
            string email = ownerEmailTextBox.Text;
            string userName = OwnerUserNameTextBox.Text;
            int phone = Convert.ToInt32(OwnerPhoneTextBox.Text);
            char type = 'c';
            string password = OwnerPasswordTextBox.Text;

            var idquery = from business in db.Businesses
                          where business.ID == Id
                          select business;

            if (idquery.Count()> 0 && ownerBusinessIdTextBox.Text != "" && email != "" && userName != "" && OwnerPhoneTextBox.Text != "" && password != "")
            {
                BusinessOwner businessOwner = new BusinessOwner
                {
                    Business_ID = Id,
                    Owner_Email = email
                };
                db.BusinessOwners.InsertOnSubmit(businessOwner);

                User usr = new User
                {
                    Email = email,
                    UserName = userName,
                    Phone = phone,
                    Type = type
                };
                db.Users.InsertOnSubmit(usr);

                UsersLogIn logIn = new UsersLogIn
                {
                    Email = email,
                    Password = password
                };
                db.UsersLogIns.InsertOnSubmit(logIn);

                db.SubmitChanges();
            }
            else {
                MessageBox.Show("one of your inputs is wrong", "");
            }
        }
    }
}
