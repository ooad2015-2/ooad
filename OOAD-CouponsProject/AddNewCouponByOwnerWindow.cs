﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class AddNewCouponByOwnerWindow : Form
    {
        BusinessOwnerClass owner;

        public AddNewCouponByOwnerWindow(BusinessOwnerClass owner)
        {
            this.owner = owner;
            InitializeComponent();
        }

        private void AddNewCouponButton_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            int businessId = Convert.ToInt32(BusinessIDTextBox.Text);
            int originalPrice = Convert.ToInt32(OriginalPriceTextBox.Text);
            int discountPrice = Convert.ToInt32(DiscountPriceTextBox.Text);
            string name = couponNamtextBox.Text;
            string description = DescriptionTextBox.Text;
            string category = CategoryComboBox.Text;
            DateTime expiredDate = ExpiredDatePicker.Value;
            int couponID = (from coupon in db.Coupons
                           select coupon).Count()+1;
            int check = (from ownerr in db.BusinessOwners
                        where ownerr.Business_ID == businessId && ownerr.Owner_Email == owner.getEmail()
                        select ownerr).Count();
            if (check > 0 && BusinessIDTextBox.Text != "" && OriginalPriceTextBox.Text != "" &&
                DiscountPriceTextBox.Text != "" && name != "" && description != "" && category != "")
            {
                Coupon couponToAdd = new Coupon
                {
                    Business_ID = businessId,
                    ID = couponID,
                    Status = "notactive",
                    Original_Price = originalPrice,
                    Name = name,
                    Description = description,
                    Category = category,
                    Discount_Price = discountPrice,
                    Expired_Date = expiredDate
                };

                db.Coupons.InsertOnSubmit(couponToAdd);
                db.SubmitChanges();

                MessageBox.Show("coupon added!");
                this.Close();
            }
            else
            {
                MessageBox.Show("one of your inputs is wrong");
            }
        }
    }
}
