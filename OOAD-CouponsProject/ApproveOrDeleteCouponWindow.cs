﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class ApproveOrDeleteCouponWindow : Form
    {
        int couponID;
        CouponToApproveWindow couponToApproveWindow;

        public ApproveOrDeleteCouponWindow(int couponID, CouponToApproveWindow couponToApproveWindow)
        {
            this.couponToApproveWindow = couponToApproveWindow;
            this.couponID = couponID;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)//approve
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            MessageBox.Show("you have approved this coupon!", "");
            Coupon couponToApprove = (from coupons in db.Coupons
                                     where coupons.ID == couponID
                                     select coupons).ToList().First();
            couponToApprove.Status = "active";
            db.SubmitChanges();
            couponToApproveWindow.setCouponsToApproveGrid();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)//delete
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            MessageBox.Show("you have deleted this coupon!", "");
            Coupon couponToDelete = (from coupons in db.Coupons
                                      where coupons.ID == couponID
                                      select coupons).ToList().First();
            couponToDelete.Status = "deleted";
            db.SubmitChanges();
            couponToApproveWindow.setCouponsToApproveGrid();
            this.Close();
        }
    }
}
