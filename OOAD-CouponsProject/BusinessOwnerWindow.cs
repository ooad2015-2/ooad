﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class BusinessOwnerWindow : Form
    {
        BusinessOwnerClass owner;

        public BusinessOwnerWindow(BusinessOwnerClass owner)
        {
            this.owner = owner;
            InitializeComponent();
        }

        private void CouponsSearchButton_Click(object sender, EventArgs e)
        {
            CouponSearchWindow couponSearchWindow = new CouponSearchWindow(owner);
            couponSearchWindow.Show();
        }

        private void OrderedCouponsButton_Click(object sender, EventArgs e)
        {
            OwnersCouponOrderesWindow orderedCouponsWindow = new OwnersCouponOrderesWindow(owner);
            orderedCouponsWindow.Show();
        }

        private void AddNewCouponButton_Click(object sender, EventArgs e)
        {
            AddNewCouponByOwnerWindow addCouponWindow = new AddNewCouponByOwnerWindow(owner);
            addCouponWindow.Show();
        }

        private void CouponsRatingsButton_Click(object sender, EventArgs e)
        {
            OwnersCouponsRatingWindow couponRatingWindow = new OwnersCouponsRatingWindow(owner);
            couponRatingWindow.Show();
        }

        private void UpdateExploitedCouponButton_Click(object sender, EventArgs e)
        {
            UpdateCouponOfCustomerWindow updateWindow = new UpdateCouponOfCustomerWindow(owner);
            updateWindow.Show();
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            ChangePasswordWindow changePasswordWindow = new ChangePasswordWindow(owner);
            changePasswordWindow.Show();
        }

        private void LogOutButton_Click(object sender, EventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void BusinessOwnerWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
        }
    }
}
