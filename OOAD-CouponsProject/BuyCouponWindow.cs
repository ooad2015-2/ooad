﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;
using System.Net.Mail;
using System.Net;

namespace OOAD_CouponsProject
{
    public partial class BuyCouponWindow : Form
    {
        CustomerClass customer;
        int CouponID;
        public BuyCouponWindow(CustomerClass customer, string couponID)
        {
            this.customer = customer;
            this.CouponID = Convert.ToInt32(couponID);

            InitializeComponent();
            int couponIDInt = Convert.ToInt32(couponID);
            DataClasses1DataContext db = new DataClasses1DataContext();
            BindingSource bindingSource1 = new BindingSource();
            var searchCoupon = from coupon in db.Coupons
                               join business in db.Businesses on coupon.Business_ID equals business.ID
                               where coupon.ID == couponIDInt
                               select new
                               {
                                   Coupon_Name = coupon.Name,
                                   Coupon_ID = coupon.ID,
                                   Category = coupon.Category,
                                   Business_Name = business.Name,
                                   Description = coupon.Description,
                                   Original_Price = coupon.Original_Price,
                                   Discount_Price = coupon.Discount_Price,
                                   Expired_Date = coupon.Expired_Date
                               };
            bindingSource1.DataSource = searchCoupon.ToList();
            dataGridView1.DataSource = bindingSource1;
        }

        private void BuyButton_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "")
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                var ordersOfTheCoupon = from order in db.Orders
                                        where order.Coupon_ID == CouponID
                                        select order;
                var selectedCoupon = from cou in db.Coupons
                                     where cou.ID == CouponID
                                     select cou;
                int nuberOfOrders = ordersOfTheCoupon.ToList().Count();
                int serialKey = Convert.ToInt32((Convert.ToString(nuberOfOrders+1)+ Convert.ToString(CouponID)));
                OrderClass orderO = new OrderClass(serialKey, DateTime.Today, "unused",
                                                   new CouponClass(selectedCoupon.First().ID, selectedCoupon.First().Name,
                                                       selectedCoupon.First().Description, selectedCoupon.First().Original_Price,
                                                       selectedCoupon.First().Discount_Price, selectedCoupon.First().Expired_Date,
                                                       selectedCoupon.First().Status));
                customer.AddNewOrder(orderO);
                Order ord = new Order
                {
                    User_Email = customer.getEmail(),
                    Price = selectedCoupon.First().Discount_Price,
                    Order_Date = DateTime.Today,
                    Serial = serialKey,
                    Coupon_ID = CouponID,
                    Status = "unused"
                };

                db.Orders.InsertOnSubmit(ord);
                db.SubmitChanges();


                try
                {
                    MailMessage message = new MailMessage();
                    SmtpClient smtp = new SmtpClient();

                    message.From = new MailAddress("couponsSystemBgu@gmail.com");
                    message.To.Add(new MailAddress(customer.getEmail()));
                    message.Subject = "Recovering Password";
                    message.Body = ord.ToString();

                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential("couponsSystemBgu@gmail.com", "bgu123456");
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(message);
                    MessageBox.Show("an Email with confirmation and coupon details has been sent to you", "");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("err: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("please enter valid details", "");
            }
        }
    }
}
