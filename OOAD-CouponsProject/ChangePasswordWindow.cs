﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class ChangePasswordWindow : Form
    {
        UserClass user;
        public ChangePasswordWindow(UserClass user)
        {
            this.user = user;
            InitializeComponent();
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            string currentPassword = CurrentPasswordBox.Text;
            string newPassword = NewPasswordBox.Text;
            DataClasses1DataContext db = new DataClasses1DataContext();

            var checkPassword = from userLogin in db.UsersLogIns
                                where userLogin.Email == user.getEmail()
                                select userLogin;
            if (checkPassword.First().Password == currentPassword)
            {
                var updateNewPassword = from userLogin in db.UsersLogIns
                                        where userLogin.Email == user.getEmail()
                                        select userLogin;
                UsersLogIn logIn = updateNewPassword.First();
                logIn.Password = newPassword;
                db.SubmitChanges();
                MessageBox.Show("You have successfully changed your password", "");
                this.Close();
            }
            else {
                MessageBox.Show("Your current password is wrong", "Wrong password");
            }
        }

    }
}
