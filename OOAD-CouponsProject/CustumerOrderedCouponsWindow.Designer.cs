﻿namespace OOAD_CouponsProject
{
    partial class CustumerOrderedCouponsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OrderedCouponsGrid = new System.Windows.Forms.DataGridView();
            this.dbDataSet = new OOAD_CouponsProject.dbDataSet();
            this.dbDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.OrderedCouponsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // OrderedCouponsGrid
            // 
            this.OrderedCouponsGrid.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.OrderedCouponsGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.OrderedCouponsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderedCouponsGrid.Location = new System.Drawing.Point(12, 45);
            this.OrderedCouponsGrid.Name = "OrderedCouponsGrid";
            this.OrderedCouponsGrid.Size = new System.Drawing.Size(400, 189);
            this.OrderedCouponsGrid.TabIndex = 0;
            // 
            // dbDataSet
            // 
            this.dbDataSet.DataSetName = "dbDataSet";
            this.dbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dbDataSetBindingSource
            // 
            this.dbDataSetBindingSource.DataSource = this.dbDataSet;
            this.dbDataSetBindingSource.Position = 0;
            // 
            // CustumerOrderedCouponsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.OrderedCouponsGrid);
            this.Name = "CustumerOrderedCouponsWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ordered Coupons";
            ((System.ComponentModel.ISupportInitialize)(this.OrderedCouponsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView OrderedCouponsGrid;
        private dbDataSet dbDataSet;
        private System.Windows.Forms.BindingSource dbDataSetBindingSource;
    }
}