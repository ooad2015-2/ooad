﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class OwnersCouponOrderesWindow : Form
    {
        BusinessOwnerClass owner;

        public OwnersCouponOrderesWindow(BusinessOwnerClass owner)
        {
            this.owner = owner;
            InitializeComponent();
            DataClasses1DataContext db = new DataClasses1DataContext();

            var orderedCoupons = from order in db.Orders
                                 join coupon in db.Coupons on order.Coupon_ID equals coupon.ID
                                 join ownerr in db.BusinessOwners on coupon.Business_ID equals ownerr.Business_ID
                                 where ownerr.Owner_Email == owner.getEmail()
                                 select new
                                 {
                                     Business_ID = ownerr.Business_ID,
                                     Coupon_ID = coupon.ID,
                                     Coupon_Name = coupon.Name,
                                     User_Email = order.User_Email,
                                     Order_SerialKey = order.Serial,
                                     Order_Date = order.Order_Date,
                                     Order_Price_Paid = order.Price,
                                     Order_Status = order.Status
                                 };
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = orderedCoupons.ToList();
            dataGridView1.DataSource = bindingSource1;
        }
    }
}
