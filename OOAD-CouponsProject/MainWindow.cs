﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;
using System.Web;

namespace OOAD_CouponsProject
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            RegisterWindow Reg = new RegisterWindow();
            Reg.Show();
        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            string City = CityBox.Text;
            string password = PasswordTextBox.Text;
            string email = EmailTextBox.Text;

            DataClasses1DataContext db = new DataClasses1DataContext();

             var queryLogin = from userLogin in db.UsersLogIns
                              where userLogin.Email == email && userLogin.Password == password
                              select userLogin;
             
             if (queryLogin.Count() != 0) //checks if the email and password is correct
             {
                 var queryUser = from user in db.Users
                                 where user.Email == email
                                 select user;
                 
                 char type = queryUser.First().Type;

                 //if the user is a customer
                 if (type == 'c')
                 {
                     if (City != "")
                     {
                         var queryOrders = from order in db.Orders
                                           where order.User_Email == queryUser.First().Email
                                           select order;
                         List<Order> orders = queryOrders.ToList();
                         List<OrderClass> userOrders = new List<OrderClass>();

                         //create customer class with list of orders
                         foreach (Order o in orders)
                         {
                             var queryCoupon = from coupon in db.Coupons
                                               where coupon.ID == o.Coupon_ID
                                               select coupon;

                             CouponClass userCoupon = new CouponClass(queryCoupon.First().ID, queryCoupon.First().Name,
                                                                      queryCoupon.First().Description, queryCoupon.First().Original_Price,
                                                                      queryCoupon.First().Discount_Price, queryCoupon.First().Expired_Date,
                                                                      queryCoupon.First().Status);
                             OrderClass ord = new OrderClass(o.Serial, o.Order_Date, o.Status, userCoupon);
                             userOrders.Add(ord);
                         }


                         CustomerClass customer = new CustomerClass(queryUser.First().Phone, queryUser.First().UserName,
                                                                    queryUser.First().Email, userOrders);
                         CustumerWindow custumerForm = new CustumerWindow(customer, City);
                         custumerForm.Show();
                         custumerForm.checkForNotifications();
                         this.Hide();
                     }
                     else
                     {
                         MessageBox.Show("Please Enter your City");
                     }
                 }

                 //if the user is a business owner
                 else if (type == 'b')
                 {
                     List<BusinessClass> listOfBusinesses = new List<BusinessClass>();
                     var businesses = from business in db.Businesses
                                      join businessOwner in db.BusinessOwners on business.ID equals businessOwner.Business_ID
                                      where businessOwner.Owner_Email == email
                                      select business;
                     foreach (var v in businesses)
                     {
                         BusinessClass bc = new BusinessClass(v.ID, v.Name, v.Address, v.City, v.Description, v.Category);
                         listOfBusinesses.Add(bc);
                     }

                     BusinessOwnerClass businessOwnerClass = new BusinessOwnerClass(listOfBusinesses, queryUser.First().Phone, queryUser.First().UserName, queryUser.First().Email);
                     BusinessOwnerWindow ownerWindow = new BusinessOwnerWindow(businessOwnerClass);
                     ownerWindow.Show();
                     this.Hide();
                 }

                 //if the user is an admin
                 else if (type == 'a')
                 {
                    
                     AdminClass adminclass = new AdminClass( queryUser.First().Phone,  queryUser.First().UserName,  queryUser.First().Email);
                     AdminWindow adminWindow = new AdminWindow(adminclass);
                     adminWindow.Show();
                     this.Hide();
                 }

             }
             else // show massage that the password or mail is incorrect
             {
                 MessageBox.Show("Incorrect Details", "Wrong Input");
             }
        }

        private void ForgetPasswordButton_Click(object sender, EventArgs e)
        {
            PasswordRecoveryWindow passwordRecoveryForm = new PasswordRecoveryWindow();
            passwordRecoveryForm.Activate();
            passwordRecoveryForm.Show();
           
            //send to email the password.
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            DateTime CurrentDate = DateTime.Now;
            DateTime LastYearDate = new DateTime(CurrentDate.Year - 1, CurrentDate.Month, CurrentDate.Day);

            var queryDelete = from Coupon in db.Coupons
                             where Coupon.Expired_Date.Date <= LastYearDate.Date
                             select Coupon;

            foreach (var Del in queryDelete)
            {
                db.Coupons.DeleteOnSubmit(Del);
            }

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // Provide for exceptions.
            }
        }

        
    }
}
