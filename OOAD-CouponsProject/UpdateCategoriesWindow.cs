﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;
namespace OOAD_CouponsProject
{
    public partial class UpdateCategoriesWindow : Form
    {
        CustomerClass customer;

        public UpdateCategoriesWindow(CustomerClass customer)
        {
            this.customer = customer;
            InitializeComponent();
            DataClasses1DataContext db = new DataClasses1DataContext();
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                
                    var query = from prefrence in db.UserPrefrences
                                where prefrence.Email == customer.getEmail() &&
                                      prefrence.Prefrence == checkedListBox1.Items[i].ToString()
                                select prefrence;
                    if (query.Count() > 0)
                    {
                        checkedListBox1.SetItemChecked(i, true);
                    }               
            }
        }

        private void UpdateCategoriesButton_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            var query = from prefrence in db.UserPrefrences
                        where customer.getEmail() == prefrence.Email
                        select prefrence;
            List<UserPrefrence> userPrefrences = query.ToList();
            List<string> prefString = new List<string>();
            foreach (UserPrefrence Prefr in userPrefrences)
            {
                prefString.Add(Prefr.Prefrence);
            }
            for (int i = 0; i < checkedListBox1.CheckedItems.Count; i++)
            {
                UserPrefrence pre = new UserPrefrence
                {
                    Email = customer.getEmail(),
                    Prefrence = checkedListBox1.CheckedItems[i].ToString()
                };
                if (!(prefString.Contains(pre.Prefrence)))
                {
                    db.UserPrefrences.InsertOnSubmit(pre);
                    db.SubmitChanges();
                }
            }
            this.Close();
        }
    }
}
