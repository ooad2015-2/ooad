﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOAD_CouponsProject
{
    public partial class ShowCouponRatingWindow : Form
    {
        int couponID;

        public ShowCouponRatingWindow(int CouponID)
        {
            this.couponID = CouponID;
            InitializeComponent();
            DataClasses1DataContext db = new DataClasses1DataContext();
            int star1 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 1 
                         select ratingg).Count();
            int star2 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 2
                         select ratingg).Count();
            int star3 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 3
                         select ratingg).Count();
            int star4 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 4
                         select ratingg).Count();
            int star5 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 5
                         select ratingg).Count();

            Star1Label.Text = Convert.ToString(star1);
            Star2Label.Text = Convert.ToString(star2);
            Star3Label.Text = Convert.ToString(star3);
            Star4Label.Text = Convert.ToString(star4);
            Star5Label.Text = Convert.ToString(star5);

            double TotalRating = (1 * star1 + 2 * star2 + 3 * star3 + 4 * star4 + 5 * star5) / (star1 + star2 + star3 + star4 + star5);
            TotalRatingLabel.Text = Convert.ToString(TotalRating);
        }

    }
}
