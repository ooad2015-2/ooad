﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OOAD_CouponsProject
{
    public partial class RegisterWindow : Form
    {
        public RegisterWindow()
        {
            InitializeComponent();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
          
            DataClasses1DataContext db = new DataClasses1DataContext();

            int phone = Convert.ToInt32(PhoneTextBox.Text);
            string Mail = EmailTextBox.Text;
            string userName = UserNameTextBox.Text;
            string password = PasswordTextBox.Text;

            var queryEmail = from userLogin in db.UsersLogIns
                             where userLogin.Email == Mail
                             select userLogin;

            if (queryEmail.Count() == 0)//check if there is no such email already exist in the system
            {
                //add user prefrences yo db
                foreach (object itemChecked in CategoryListBox.CheckedItems)
                {
                    UserPrefrence UP = new UserPrefrence
                    {
                        Prefrence = itemChecked.ToString(),
                        Email = Mail
                    };
                    db.UserPrefrences.InsertOnSubmit(UP);
                }
                //add new user to db
                UsersLogIn newUser = new UsersLogIn
                {
                    Email = Mail,
                    Password = password
                };


                db.UsersLogIns.InsertOnSubmit(newUser);

                User Tmp = new User
                {
                    Email = Mail,
                    Phone = phone,
                    UserName = userName,
                    Type = 'c'
                };
                db.Users.InsertOnSubmit(Tmp);


                try
                {
                    db.SubmitChanges();
                    //  this.Close();
                }
                catch (Exception ex)
                {
                    //massagebox pop to alert that he didnt sign in
                    Console.WriteLine(ex);
                    db.SubmitChanges();
                }

                this.Close();
            }
            else
            {
                MessageBox.Show("E-mail is already exist in the system", "Wrong Input");
            }
            
        }

    }
}
