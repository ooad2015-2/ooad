﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOAD_CouponsProject
{
    public partial class AddBusinessWindow : Form
    {
        public AddBusinessWindow()
        {
            InitializeComponent();
        }

        private void AddBusinessButton_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            if (BusinessAdressTextBox.Text != "" && BusinessDescriptionTextBox.Text != "" && BusinessIDTextBox.Text != "" &&
                BusinessNameTextBox.Text != "" && BusinessCategoryComboBox.Text != "" && BusinessCityComboBox.Text != "")
            {
                Business business = new Business
                {
                    ID = Convert.ToInt32(BusinessIDTextBox.Text),
                    Address = BusinessAdressTextBox.Text,
                    City = BusinessCityComboBox.Text,
                    Name = BusinessNameTextBox.Text,
                    Description = BusinessDescriptionTextBox.Text,
                    Category = BusinessCategoryComboBox.Text
                };

                db.Businesses.InsertOnSubmit(business);
                db.SubmitChanges();
                MessageBox.Show("Business added");
                this.Close();

            }
            else
            {
                MessageBox.Show("one of your inputs is wrong", "");
            }
        }
    }
}
