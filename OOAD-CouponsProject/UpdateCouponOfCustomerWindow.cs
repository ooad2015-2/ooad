﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class UpdateCouponOfCustomerWindow : Form
    {
        BusinessOwnerClass owner;

        public UpdateCouponOfCustomerWindow(BusinessOwnerClass owner)
        {
            this.owner = owner;
            InitializeComponent();
        }

        private void UpdateCoustomerCouponButton_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            Order order = (from ordr in db.Orders
                           join coupon in db.Coupons on ordr.Coupon_ID equals coupon.ID
                           join bOwner in db.BusinessOwners on coupon.Business_ID equals bOwner.Business_ID
                           where ordr.Serial == Convert.ToInt32(SerialKeyTextBox.Text) && ordr.Status == "unused" &&
                                 bOwner.Owner_Email == owner.getEmail()
                           select ordr).First();

            if (order != null)
            {
                order.Status = "used";
                db.SubmitChanges();
                MessageBox.Show("customer coupon marked as used!");
                this.Close();
            }
            else {
                MessageBox.Show("this serial is not exist or already used");
            }
        }
    }
}
