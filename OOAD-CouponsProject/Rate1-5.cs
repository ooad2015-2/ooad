﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class Rate1_5 : Form
    {
        CustomerClass customer;
        int couponId;
        RateCouponsWindow rateCouponWindow;

        public Rate1_5(CustomerClass customer, int couponID,RateCouponsWindow rateCouponWindow)
        {
            this.customer = customer;
            this.couponId = couponID;
            this.rateCouponWindow = rateCouponWindow;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 1 stars", "");
            updateRatingInDB(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 2 stars", "");
            updateRatingInDB(2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 3 stars", "");
            updateRatingInDB(3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 4 stars", "");
            updateRatingInDB(4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 5 stars", "");
            updateRatingInDB(5);
        }

        private void updateRatingInDB(int ratingg)
        {
            
            rateCouponWindow.setCouponsToRate();
            DataClasses1DataContext db = new DataClasses1DataContext();
            Rating rate = new Rating 
            {
                Email = customer.getEmail(),
                Coupon_ID = couponId,
                Rating1 = ratingg
            };

            db.Ratings.InsertOnSubmit(rate);
            db.SubmitChanges();
            rateCouponWindow.setCouponsToRate();

            this.Close();
        }
    }
}
