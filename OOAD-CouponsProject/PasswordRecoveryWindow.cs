﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;
using System.Web;


namespace OOAD_CouponsProject
{
    public partial class PasswordRecoveryWindow : Form
    {
        public PasswordRecoveryWindow()
        {
            InitializeComponent();
        }

        private void RecoverPasswordButton_Click(object sender, EventArgs e)
        {

            string toEmail = RecoveryEmailBox.Text;

            DataClasses1DataContext db = new DataClasses1DataContext();

            var queryLogin = from userLogin in db.UsersLogIns
                             where userLogin.Email == toEmail
                             select userLogin;

            MessageBox.Show(queryLogin.First().Password);

            if (queryLogin.Count() != 0)
            {
                string pass = queryLogin.First().Password;
                MessageBox.Show("Yes");

                try
                {
                    MailMessage message = new MailMessage();
                    SmtpClient smtp = new SmtpClient();

                    message.From = new MailAddress("couponsSystemBgu@gmail.com");
                    message.To.Add(new MailAddress(toEmail));
                    message.Subject = "Recovering Password";
                    message.Body = pass;

                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential("couponsSystemBgu@gmail.com", "bgu123456");
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(message);
                    MessageBox.Show("an E-mail with your password has sent to your E-mail.", "Recover Password");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("err: " + ex.Message);
                }
                this.Close();
            }

            MessageBox.Show("This mail address dose no exist in the system");
            this.Close();
        }
    }
}
