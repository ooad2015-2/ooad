﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class CouponSearchWindow : Form
    {

        UserClass user;

        public CouponSearchWindow(UserClass user)
        {
            this.user = user;
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            string city = CityBox.Text;
            string category = CategoryBox.Text;
            DataClasses1DataContext db = new DataClasses1DataContext();
            BindingSource bindingSource1 = new BindingSource();
            if (city != "" && category != "")
            {
                var searchCoupon = from coupon in db.Coupons
                                   join business in db.Businesses on coupon.Business_ID equals business.ID
                                   where coupon.Category == category && coupon.Status == "active"
                                         && business.City == city && coupon.Expired_Date > DateTime.Today
                                   select new
                                   {
                                       Coupon_Name = coupon.Name,
                                       Coupon_ID = coupon.ID,
                                       Category = coupon.Category,
                                       Business_Name = business.Name,
                                       Description = coupon.Description,
                                       Original_Price = coupon.Original_Price,
                                       Discount_Price = coupon.Discount_Price,
                                       Expired_Date = coupon.Expired_Date
                                   };
                bindingSource1.DataSource = searchCoupon.ToList();
                SearchCouponGrid.DataSource = bindingSource1;
            }
            else if(city == "" && category != ""){
                var searchCoupon = from coupon in db.Coupons
                                   join business in db.Businesses on coupon.Business_ID equals business.ID
                                   where coupon.Category == category && coupon.Status == "active"
                                         && coupon.Expired_Date > DateTime.Today
                                   select new
                                   {
                                       Coupon_Name = coupon.Name,
                                       Coupon_ID = coupon.ID,
                                       Category = coupon.Category,
                                       Business_Name = business.Name,
                                       Description = coupon.Description,
                                       Original_Price = coupon.Original_Price,
                                       Discount_Price = coupon.Discount_Price,
                                       Expired_Date = coupon.Expired_Date
                                   };
                bindingSource1.DataSource = searchCoupon.ToList();
                SearchCouponGrid.DataSource = bindingSource1;
            }
            else if (city != "" && category == "") {
                var searchCoupon = from coupon in db.Coupons
                                   join business in db.Businesses on coupon.Business_ID equals business.ID
                                   where business.City == city && coupon.Status == "active"
                                         && coupon.Expired_Date > DateTime.Today
                                   select new
                                   {
                                       Coupon_Name = coupon.Name,
                                       Coupon_ID = coupon.ID,
                                       Category = coupon.Category,
                                       Business_Name = business.Name,
                                       Description = coupon.Description,
                                       Original_Price = coupon.Original_Price,
                                       Discount_Price = coupon.Discount_Price,
                                       Expired_Date = coupon.Expired_Date
                                   };
                bindingSource1.DataSource = searchCoupon.ToList();
                SearchCouponGrid.DataSource = bindingSource1;
            }
            else if (city == "" && category == "") {
                var searchCoupon = from coupon in db.Coupons
                                   join business in db.Businesses on coupon.Business_ID equals business.ID
                                   where  coupon.Status == "active" && coupon.Expired_Date > DateTime.Today
                                   select new
                                   {
                                       Coupon_Name = coupon.Name,
                                       Coupon_ID = coupon.ID,
                                       Category = coupon.Category,
                                       Business_Name = business.Name,
                                       Description = coupon.Description,
                                       Original_Price = coupon.Original_Price,
                                       Discount_Price = coupon.Discount_Price,
                                       Expired_Date = coupon.Expired_Date
                                   };
                bindingSource1.DataSource = searchCoupon.ToList();
                SearchCouponGrid.DataSource = bindingSource1;
            }
        }

        private void SearchCouponGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (user.GetType() == typeof(CustomerClass))
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = SearchCouponGrid.Rows[rowIndex];
                BuyCouponWindow buycouponWindow = new BuyCouponWindow((CustomerClass)user, row.Cells[1].Value.ToString());
                buycouponWindow.Show();
            }
            else if (user.GetType() == typeof(AdminClass))
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = SearchCouponGrid.Rows[rowIndex];
                EditCouponWindow editCouponWindow = new EditCouponWindow((AdminClass)user, row.Cells[1].Value.ToString());
                editCouponWindow.Show();
            }
        }

    }
}
