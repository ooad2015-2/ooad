﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class CouponToApproveWindow : Form
    {
        public CouponToApproveWindow()
        {
            InitializeComponent();
            setCouponsToApproveGrid();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ApproveOrDeleteCouponWindow appOrDel = new ApproveOrDeleteCouponWindow(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString()), this);
            appOrDel.Show();
        }

        public void setCouponsToApproveGrid()
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            BindingSource bindingSource1 = new BindingSource();
            var couponsWaitingForAprovel = from coupon in db.Coupons
                                           join business in db.Businesses on coupon.Business_ID equals business.ID
                                           where coupon.Status == "notactive"
                                           select new
                                           {
                                               Coupon_Name = coupon.Name,
                                               Coupon_ID = coupon.ID,
                                               Category = coupon.Category,
                                               Business_Name = business.Name,
                                               Description = coupon.Description,
                                               Original_Price = coupon.Original_Price,
                                               Discount_Price = coupon.Discount_Price,
                                               Expired_Date = coupon.Expired_Date
                                           };
            bindingSource1.DataSource = couponsWaitingForAprovel.ToList();
            dataGridView1.DataSource = bindingSource1;
        }
    }
}
