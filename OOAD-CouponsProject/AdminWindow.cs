﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class AdminWindow : Form
    {
        AdminClass admin;

        public AdminWindow(AdminClass admin)
        {
            this.admin = admin;
            InitializeComponent();
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            ChangePasswordWindow changePasswordWindow = new ChangePasswordWindow(admin);
            changePasswordWindow.Show();
        }

        private void CouponListButton_Click(object sender, EventArgs e)
        {
            CouponSearchWindow couponSearchWindow = new CouponSearchWindow(admin);
            couponSearchWindow.Show();
        }

        private void BusinessesListButton_Click(object sender, EventArgs e)
        {
            BusinessSearchWindow businessSearchWindow = new BusinessSearchWindow(admin);
            businessSearchWindow.Show();
        }

        private void CouponsApprovalButton_Click(object sender, EventArgs e)
        {
            CouponToApproveWindow couponToApprove = new CouponToApproveWindow();
            couponToApprove.Show();
        }

        private void AddNewBusinessButton_Click(object sender, EventArgs e)
        {
            AddBusinessWindow addBusinessWindow = new AddBusinessWindow();
            addBusinessWindow.Show();
        }

        private void AddNewBusinessOwnerButton_Click(object sender, EventArgs e)
        {
            AddBusinessOwnerWindow addBusinessOwnerWindow = new AddBusinessOwnerWindow();
            addBusinessOwnerWindow.Show();
        }

        private void LogOutButton_Click(object sender, EventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void AdminWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
        }
    }
}
