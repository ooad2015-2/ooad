﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;


namespace OOAD_CouponsProject
{
    public partial class DeleteBusinessWindow : Form
    {
        AdminClass Admin;
        string BusinessID;
        public DeleteBusinessWindow(AdminClass Admin, string BusinessID)
        {
            this.Admin = Admin;
            this.BusinessID = BusinessID;
            InitializeComponent();

            int couponIDInt = Convert.ToInt32(BusinessID);
            DataClasses1DataContext db = new DataClasses1DataContext();
            BindingSource bindingSource1 = new BindingSource();
            var DeleteBusiness = from business in db.Businesses
                               where business.ID == couponIDInt
                               select new
                               {
                                   Business_ID = business.ID,
                                   Business_Name = business.Name,
                                   Category = business.Category,
                                   Description = business.Description,
                                   Address = business.Address,
                                   City = business.City
                               };
            bindingSource1.DataSource = DeleteBusiness.ToList();
            dataGridView1.DataSource = bindingSource1;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            var DeleteBusiness = from business in db.Businesses
                                 where business.ID == Convert.ToInt32(BusinessID)
                                 select business;
            var DeleteBusinesses = from business in db.BusinessOwners
                                 where business.Business_ID == Convert.ToInt32(BusinessID)
                                 select business;
            var DeleteCoupons = from coupon in db.Coupons
                                   where coupon.Business_ID == Convert.ToInt32(BusinessID)
                                   select coupon;
            /*
            foreach (var Del in DeleteCoupons)
            {
                db.Coupons.DeleteOnSubmit(Del);
                // MessageBox.Show("ggg");
            }

            foreach (var Del in DeleteBusinesses)
            {
                db.BusinessOwners.DeleteOnSubmit(Del);
                // MessageBox.Show("ggg");
            }
*/
            foreach (var Del in DeleteBusiness)
            {
                db.Businesses.DeleteOnSubmit(Del);
                
            }

            try
            {
                db.SubmitChanges();
                MessageBox.Show("Business has been Deleted");

            }
              catch (Exception ex)
            {
                Console.WriteLine(ex);
              //   Provide for exceptions.
            }
        }
    }
}
