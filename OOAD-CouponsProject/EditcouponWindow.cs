﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class EditCouponWindow : Form
    {
        AdminClass Admin;
        string CouponID;
        public EditCouponWindow(AdminClass Admin, String CouponID)
        {
            this.Admin = Admin;
            this.CouponID = CouponID;
            InitializeComponent();
            int couponIDInt = Convert.ToInt32(CouponID);
            DataClasses1DataContext db = new DataClasses1DataContext();
            BindingSource bindingSource1 = new BindingSource();
            var searchCoupon = from coupon in db.Coupons
                               join business in db.Businesses on coupon.Business_ID equals business.ID
                               where coupon.ID == couponIDInt
                               select new
                               {
                                   Coupon_Name = coupon.Name,
                                   Coupon_ID = coupon.ID,
                                   Category = coupon.Category,
                                   Business_Name = business.Name,
                                   Description = coupon.Description,
                                   Original_Price = coupon.Original_Price,
                                   Discount_Price = coupon.Discount_Price,
                                   Expired_Date = coupon.Expired_Date
                               };
            bindingSource1.DataSource = searchCoupon.ToList();
            dataGridView1.DataSource = bindingSource1;
        }

        private void ChangeCouponButton_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            var FindCoupon = from coupon in db.Coupons
                             where coupon.ID == Convert.ToInt32(CouponID)
                             select coupon;
            Coupon c = FindCoupon.First();


            if (CouponNameTextBox.Text != "")
            {
                c.Name = CouponNameTextBox.Text;
            }
            if (CategoryBox.Text != "")
            {
                c.Category = CategoryBox.Text;

            }
            if (DescriptiontextBox.Text != "")
            {
                c.Description = DescriptiontextBox.Text;

            }
            if (DiscountTextBox.Text != "")
            {
                c.Discount_Price = Convert.ToInt32(DiscountTextBox.Text);

            }
            if (ExpireDatePicker.Value.Date != DateTime.Now.Date)
            {
                c.Expired_Date = ExpireDatePicker.Value;

            }

            db.SubmitChanges();

            MessageBox.Show("Changes Succesfully Submited");
            this.Close();
        }
    }
}
