﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class OwnersCouponsRatingWindow : Form
    {
        BusinessOwnerClass owner;

        public OwnersCouponsRatingWindow(BusinessOwnerClass owner)
        {
            this.owner = owner;
            InitializeComponent();
            DataClasses1DataContext db = new DataClasses1DataContext();

            var allCoupons = from coupon in db.Coupons
                             join ownerr in db.BusinessOwners on coupon.Business_ID equals ownerr.Business_ID
                             where ownerr.Owner_Email == owner.getEmail() && coupon.Status == "active"
                             select new
                             {
                                 Business_ID = coupon.Business_ID,
                                 Coupon_ID = coupon.ID,
                                 Coupon_Name = coupon.Name,
                                 Original_Price = coupon.Original_Price,
                                 Discount_Price = coupon.Discount_Price,
                                 Category = coupon.Category,
                                 Description = coupon.Description,
                                 Expired_Date = coupon.Expired_Date
                             };

            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = allCoupons.ToList();
            dataGridView1.DataSource = bindingSource1;
            
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ShowCouponRatingWindow ratingWindow = new ShowCouponRatingWindow(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString()));
            ratingWindow.Show();
        }
    }
}
