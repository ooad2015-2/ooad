﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class CustumerOrderedCouponsWindow : Form
    {
        CustomerClass customer;

        public CustumerOrderedCouponsWindow(CustomerClass customer)
        {
            this.customer = customer;
            InitializeComponent();

            DataClasses1DataContext db = new DataClasses1DataContext();
            BindingSource bindingSource1 = new BindingSource();
                var orderedCoupons = from coupon in db.Coupons
                                     join business in db.Businesses on coupon.Business_ID equals business.ID
                                     join order in db.Orders on coupon.ID equals order.Coupon_ID
                                     where order.User_Email == customer.getEmail()
                                     select new
                                   {
                                       Serial = order.Serial,
                                       Coupon_Name = coupon.Name,
                                       Category = coupon.Category,
                                       Business_Name = business.Name,
                                       Description = coupon.Description,
                                       Status = order.Status,
                                       Order_Date = order.Order_Date,
                                       Price_Paid = order.Price,
                                       Expired_Date = coupon.Expired_Date
                                   };
                bindingSource1.DataSource = orderedCoupons.ToList();
                OrderedCouponsGrid.DataSource = bindingSource1;
            
        }


    }
}
