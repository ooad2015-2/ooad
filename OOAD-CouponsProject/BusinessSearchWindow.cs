﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class BusinessSearchWindow : Form
    {
        UserClass User;        
        public BusinessSearchWindow(UserClass user)
        {
            User = user;
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            string city = CityBox.Text;
            string category = CategoryBox.Text;
            DataClasses1DataContext db = new DataClasses1DataContext();
            BindingSource bindingSource1 = new BindingSource();
            if (city != "" && category != "")
            {
                var searchBusiness = from business in db.Businesses
                                     where business.Category == category && business.City == city
                                     select new
                                   {
                                       Business_ID = business.ID,
                                       Business_Name = business.Name,
                                       Category = business.Category,
                                       Description = business.Description,
                                       Address = business.Address,
                                       City = business.City
                                   };
                bindingSource1.DataSource = searchBusiness.ToList();
                SearchBusinessGrid.DataSource = bindingSource1;
            }
            else if(city == "" && category != ""){
                var searchBusiness = from business in db.Businesses
                                     where business.Category == category 
                                     select new
                                     {
                                         Business_ID = business.ID,
                                         Business_Name = business.Name,
                                         Category = business.Category,
                                         Description = business.Description,
                                         Address = business.Address,
                                         City = business.City
                                     };
                bindingSource1.DataSource = searchBusiness.ToList();
                SearchBusinessGrid.DataSource = bindingSource1;
            }
            else if (city != "" && category == "") {
                var searchBusiness = from business in db.Businesses
                                     where business.City == city
                                     select new
                                     {
                                         Business_ID = business.ID,
                                         Business_Name = business.Name,
                                         Category = business.Category,
                                         Description = business.Description,
                                         Address = business.Address,
                                         City = business.City
                                     };
                bindingSource1.DataSource = searchBusiness.ToList();
                SearchBusinessGrid.DataSource = bindingSource1;
            }
            else if (city == "" && category == "") {
                var searchBusiness = from business in db.Businesses
                                     select new
                                     {
                                         Business_ID = business.ID,
                                         Business_Name = business.Name,
                                         Category = business.Category,
                                         Description = business.Description,
                                         Address = business.Address,
                                         City = business.City
                                     };
                bindingSource1.DataSource = searchBusiness.ToList();
                SearchBusinessGrid.DataSource = bindingSource1;
            }
        }

        private void SearchBusinessGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (User.GetType() == typeof(AdminClass))
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = SearchBusinessGrid.Rows[rowIndex];
                DeleteBusinessWindow deleteBusinessWindow = new DeleteBusinessWindow((AdminClass)User, row.Cells[0].Value.ToString());
                deleteBusinessWindow.Show();
            }
        }


    }
}
