﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL_BackEnd;

namespace OOAD_CouponsProject
{
    public partial class RateCouponsWindow : Form
    {
        CustomerClass customer;

        public RateCouponsWindow(CustomerClass customer)
        {
            this.customer = customer;
            InitializeComponent();
            setCouponsToRate();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            Rate1_5 rate = new Rate1_5(customer, Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString()),this);
            rate.Show();

        }
        public void setCouponsToRate()
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            BindingSource bindingSource1 = new BindingSource();
            var usedCoupons = from coupon in db.Coupons
                              join business in db.Businesses on coupon.Business_ID equals business.ID
                              join order in db.Orders on coupon.ID equals order.Coupon_ID
                              where order.User_Email == customer.getEmail() && order.Status == "used" && !db.Ratings.Any(r => r.Coupon_ID == order.Coupon_ID)
                              select new
                              {
                                  Coupon_Name = coupon.Name,
                                  Coupon_ID = coupon.ID,
                                  Category = coupon.Category,
                                  Business_Name = business.Name,
                                  Description = coupon.Description,
                                  Order_Date = order.Order_Date
                              };

            bindingSource1.DataSource = usedCoupons.ToList();
            dataGridView1.DataSource = bindingSource1;
        }
    }
}
