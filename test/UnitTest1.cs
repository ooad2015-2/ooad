﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using OOAD_CouponsProject;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Transactions;

namespace test
{
    [TestClass]
    public class UnitTest1
    {
        TransactionScope _tx; 
        [TestInitialize]
        public void Init()
        {
            _tx = new TransactionScope();
        }

        [TestCleanup]
        public void CleanUp()
        {
            _tx.Dispose();
        }



        [TestMethod]
        public void TestExist1()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                Boolean exist = false;
                foreach (Business b in db.Businesses)
                {
                    if (b.ID.Equals(9999))
                        exist = true;
                }
                Assert.IsFalse(exist);
            }
        }

        [TestMethod]
        public void TestInsert2()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                Business bs = new Business
                {
                    ID = 9999,
                    Address = "nesher",
                    City = "nesher",
                    Name = "Daniel",
                    Description = "hhhh",
                    Category = "wwwww"
                };

                db.Businesses.InsertOnSubmit(bs);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    db.SubmitChanges();
                }
                Business check = null;
                foreach (Business bu in db.Businesses)
                {
                    if (bu.ID.Equals(9999))
                    {
                        check = bu;
                    }
                }
                Assert.AreEqual(check.ID, 9999);
            }
        }

        [TestMethod]
        public void TestInsert3()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                int count = db.Businesses.Count();
                int check = 0;
                foreach (Business bu in db.Businesses)
                {
                    check++;
                }
                Assert.AreEqual(check, count);
            }
        }

        [TestMethod]
        public void TestDeleteAllRowsFromTable4()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                foreach (UserPrefrence ur in db.UserPrefrences)
                {
                    db.UserPrefrences.DeleteOnSubmit(ur);
                }
                db.SubmitChanges();
                int count = db.UserPrefrences.Count();
                Assert.AreEqual(count, 0);
            }
        }

        [TestMethod]
        public void TestChangePriceInCoupon5()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                int oldPrice = -1;
                Coupon coupon = null;
                foreach (Coupon c in db.Coupons)
                {
                    if (c.ID.Equals(123456))
                    {
                        coupon = c;
                        oldPrice = c.Discount_Price;
                        c.Discount_Price = oldPrice / 2;
                    }
                }
                db.SubmitChanges();
                Assert.AreEqual(oldPrice / 2, coupon.Discount_Price);
            }
        }

        [TestMethod]
        public void TestInsert6()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                User usr = new User
                {
                    Email = "Daniel@gmail.com",
                    UserName = "Daniel",
                    Phone = 529400407,
                    Type = 'a'
                };
                int countUsers = db.Users.Count();
                db.Users.InsertOnSubmit(usr);
                db.SubmitChanges();
                int count = db.Users.Count();
                Assert.AreNotEqual(countUsers, count);
            }
        }

        [TestMethod]
        public void TestInsertAndDelete7()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                int countRows = db.Users.Count();
                User usr = new User
                {
                    Email = "ShayDayan@gmail.com",
                    UserName = "Shay",
                    Phone = 566666666,
                    Type = 'b'
                };
                db.Users.InsertOnSubmit(usr);

                try
                {
                    db.SubmitChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    db.SubmitChanges();
                }
                User usr2 = null;
                foreach (User u in db.Users)
                {
                    if (u.Email.Equals("ShayDayan@gmail.com"))
                        usr2 = u;
                }
                db.Users.DeleteOnSubmit(usr2);
                db.SubmitChanges();
                int count = db.Users.Count();
                Assert.AreEqual(countRows, count);
            }
        }

        [TestMethod]
        public void TestDelete8()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                Business bs = new Business
                {
                    ID = 9999,
                    Address = "nesher",
                    City = "nesher",
                    Name = "Daniel",
                    Description = "hhhh",
                    Category = "wwwww"
                };
                db.Businesses.InsertOnSubmit(bs);
                Coupon cp = new Coupon
                {
                    Business_ID = 9999,
                    ID = 77474747,
                    Status = "active",
                    Original_Price = 5000,
                    Name = "Something good",
                    Category = "funny",
                    Discount_Price = 3500,
                    Description = "wwww",
                    Expired_Date = new DateTime(2015,08,15)   
                };
                db.Coupons.InsertOnSubmit(cp);
                db.SubmitChanges();
                Business b = null;
                foreach (Business busns in db.Businesses)
                {
                    if (busns.ID.Equals(9999))
                        b = busns;
                }
                db.Businesses.DeleteOnSubmit(b);
                try
                {
                    db.SubmitChanges();
                    Assert.Fail("Should have exceptioned above!");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                
            }
        }

        [TestMethod]
        public void TestSumOrdersOfCoupon9()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                Business bs = new Business
                {
                    ID = 9999,
                    Address = "nesher",
                    City = "nesher",
                    Name = "Daniel",
                    Description = "hhhh",
                    Category = "wwwww"
                };
                db.Businesses.InsertOnSubmit(bs);
                Coupon cp = new Coupon
                {
                    Business_ID = 9999,
                    ID = 1234567,
                    Status = "active",
                    Original_Price = 5000,
                    Name = "Something good",
                    Category = "funny",
                    Discount_Price = 3500,
                    Description = "wwww",
                    Expired_Date = new DateTime(2015, 08, 15)
                };
                db.Coupons.InsertOnSubmit(cp);
                User usr1 = new User
                {
                    Email = "Daniel@gmail.com",
                    UserName = "Daniel",
                    Phone = 529400407,
                    Type = 'c'
                };
                db.Users.InsertOnSubmit(usr1);
                User usr2 = new User
                {
                    Email = "efi@gmail.com",
                    UserName = "efi",
                    Phone = 52100000,
                    Type = 'c'
                };
                db.Users.InsertOnSubmit(usr2);
                Order ord1 = new Order
                {
                    User_Email = "Daniel@gmail.com",
                    Coupon_ID = 1234567,
                    Serial = 8877894,
                    Order_Date = new DateTime(2015, 04, 17),
                    Price = 3500,
                    Status = "used"
                };
                db.Orders.InsertOnSubmit(ord1);
                Order ord2 = new Order
                {
                    User_Email = "efi@gmail.com",
                    Coupon_ID = 1234567,
                    Serial = 8877895,
                    Order_Date = new DateTime(2015, 04, 17),
                    Price = 3500,
                    Status = "used"
                };
                db.Orders.InsertOnSubmit(ord2);
                db.SubmitChanges();
                var queryCouponOrder = from ord in db.Orders
                                          where ord.Coupon_ID.Equals(1234567)
                                          select ord;
                int count = queryCouponOrder.Count();
                Assert.AreEqual(2, count);
            }
        }

        [TestMethod]
        public void TestInsertTheSamePrimaryKey10()
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                Business bs1 = new Business
                {
                    ID = 9999,
                    Address = "nesher",
                    City = "nesher",
                    Name = "Daniel",
                    Description = "hhhh",
                    Category = "wwwww"
                };
                db.Businesses.InsertOnSubmit(bs1);
                Business bs2 = new Business
                {
                    ID = 9999,
                    Address = "beer sheva",
                    City = "beer sheva",
                    Name = "Liora",
                    Description = "hhhh",
                    Category = "wwwww"
                };
                try
                {
                    db.Businesses.InsertOnSubmit(bs2);
                    Assert.Fail("Should have exceptioned above!");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
